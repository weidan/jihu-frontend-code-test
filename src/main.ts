import 'uno.css'
import '@unocss/reset/tailwind.css'
import '@fontsource/source-sans-pro'
import './styles/global.styl'
import { createRouter, createWebHashHistory, RouterView } from 'vue-router'
import { createApp, h } from 'vue'

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      name: 'home',
      path: '/',
      component: () => import('./pages/home/app.vue'),
    },
    {
      name: 'problem-1',
      path: '/problem-1',
      component: () => import('./pages/problem-1/app.vue'),
    },
    {
      name: 'problem-2',
      path: '/problem-2',
      component: () => import('./pages/problem-2/app.vue'),
    },
  ],
})

const app = createApp({ render: () => h(RouterView) })
app.use(router)
app.mount('#app')
