interface RectanglePosition {
  left: number
  top: number
}

interface RectangleSize {
  width: number
  height: number
}

export function isOverlap({
  basePosition,
  baseSize,
  targetPosition,
  targetSize,
}: {
  basePosition: RectanglePosition
  baseSize: RectangleSize
  targetPosition: RectanglePosition
  targetSize: RectangleSize
}) {
  const isOverlapInX =
    Math.min(
      basePosition.left + baseSize.width,
      targetPosition.left + targetSize.width
    ) > Math.max(basePosition.left, targetPosition.left)
  const isOverlapInY =
    Math.min(
      basePosition.top + baseSize.height,
      targetPosition.top + targetSize.height
    ) > Math.max(basePosition.top, targetPosition.top)
  return isOverlapInX && isOverlapInY
}
