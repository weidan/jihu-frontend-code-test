import type { VueWrapper, DOMWrapper } from '@vue/test-utils'
import type { Router } from 'vue-router'
import { describe, expect, test, beforeEach } from 'vitest'
import { h } from 'vue'
import { mount } from '@vue/test-utils'
import { createRouter, createWebHistory, RouterView } from 'vue-router'
import App from '../src/pages/problem-2/app.vue'
import testListData from '../src/assets/test.json'

const { data: list } = testListData

describe('project 2', () => {
  let router: Router
  const routes = [{ name: 'problem-2', path: '/problem-2', component: App }]
  let wrapper: VueWrapper
  let itemCard: DOMWrapper<HTMLDivElement>

  beforeEach(async () => {
    router = createRouter({ history: createWebHistory(), routes })
    router.replace({ name: 'problem-2' })
    await router.isReady()
    wrapper = mount(
      { render: () => h(RouterView) },
      { global: { plugins: [router] } }
    )
    itemCard = wrapper.find('.item-card')
  })

  test('filter items', async () => {
    expect(wrapper.findAll('.item-card')).toHaveLength(list.length)

    const input = wrapper.find('input')
    const form = wrapper.find('form')
    input.setValue('Lorem ipsum')
    form.trigger('submit')
    await sleep()

    expect(wrapper.findAll('.item-card')).toHaveLength(1)
  })

  test('add an item tag', async () => {
    let tags = itemCard.findAll('.tag-text').map((el) => el.text())
    expect(tags).toEqual(['tag1', 'tag2', 'tag3'])

    itemCard.find('.add-tag-button').trigger('click')
    await wrapper.vm.$nextTick()
    itemCard.find('input').setValue('new tag')
    itemCard.find('form').trigger('submit')
    await wrapper.vm.$nextTick()

    tags = itemCard.findAll('.tag-text').map((el) => el.text())
    expect(tags).toEqual(['tag1', 'tag2', 'tag3', 'new tag'])
  })

  test('remove an item tag', async () => {
    let tags = itemCard.findAll('.tag-text').map((el) => el.text())
    expect(tags).toEqual(['tag1', 'tag2', 'tag3'])

    itemCard.find('.remove-tag-button').trigger('click')
    await wrapper.vm.$nextTick()
    tags = itemCard.findAll('.tag-text').map((el) => el.text())
    expect(tags).toEqual(['tag2', 'tag3'])
  })
})

async function sleep(timeMs = 0) {
  return await new Promise(function (resolve) {
    setTimeout(() => resolve(true), timeMs)
  })
}
