import type { DOMWrapper } from '@vue/test-utils'
import { describe, expect, test } from 'vitest'
import { mount } from '@vue/test-utils'
import App from '../src/pages/problem-1/app.vue'

describe('problem 1', () => {
  test('move blocks', async () => {
    const wrapper = mount(App)

    const slotContainer = wrapper.find<HTMLDivElement>('.slot-container')
    expect(getPosition(slotContainer)).toEqual({ x: 0, y: 0 })

    moveBlockTo(slotContainer, { x: 200, y: 200 })
    await wrapper.vm.$nextTick()
    expect(getPosition(slotContainer)).toEqual({ x: 200, y: 200 })
  })

  test('move block to slot', async () => {
    const wrapper = mount(App)

    const slotContainer = wrapper.find<HTMLDivElement>('.slot-container')
    const blockContainer = wrapper.find<HTMLDivElement>('.block-container')

    moveBlockTo(slotContainer, { x: 200, y: 200 })
    moveBlockTo(blockContainer, { x: 250, y: 250 })
    await wrapper.vm.$nextTick()
    expect(getPosition(slotContainer)).toEqual({ x: 200, y: 200 })
    expect(getPosition(blockContainer)).toEqual({ x: 200, y: 200 })
  })

  test('move slot to block', async () => {
    const wrapper = mount(App)

    const slotContainer = wrapper.find<HTMLDivElement>('.slot-container')
    const blockContainer = wrapper.find<HTMLDivElement>('.block-container')
    const block = wrapper.find<HTMLDivElement>('.block')

    expect(block.classes()).not.toContain('synced')
    moveBlockTo(blockContainer, { x: 100, y: 100 })
    moveBlockTo(slotContainer, { x: 50, y: 50 })
    await wrapper.vm.$nextTick()
    expect(getPosition(slotContainer)).toEqual({ x: 100, y: 100 })
    expect(getPosition(blockContainer)).toEqual({ x: 100, y: 100 })
    expect(block.classes()).toContain('synced')
  })

  test('highlight slot if there are any overlapped areas', async () => {
    const wrapper = mount(App)

    const slotContainer = wrapper.find<HTMLDivElement>('.slot-container')
    const slot = wrapper.find<HTMLDivElement>('.slot')
    moveBlockTo(slotContainer, { x: 0, y: 0 })

    const blockContainer = wrapper.find<HTMLDivElement>('.block-container')
    blockContainer.trigger('mousedown', {
      clientX: getPosition(blockContainer).x,
      clientY: getPosition(blockContainer).y,
    })

    expect(slot.classes()).not.toContain('highlighted')

    document.dispatchEvent(
      new MouseEvent('mousemove', { clientX: 50, clientY: 50 })
    )
    await wrapper.vm.$nextTick()
    expect(slot.classes()).toContain('highlighted')

    document.dispatchEvent(
      new MouseEvent('mousemove', { clientX: 250, clientY: 250 })
    )
    await wrapper.vm.$nextTick()
    expect(slot.classes()).not.toContain('highlighted')
  })
})

function getPosition(domWrapper: DOMWrapper<HTMLElement>) {
  const { transform } = domWrapper.element.style
  if (!transform) return { x: 0, y: 0 }

  const [x, y] = transform
    .match(/\(.+\)/)[0]
    .slice(1, -1)
    .split(/,/)
    .slice(0, 2)
    .map((number) => parseFloat(number))
  return { x, y }
}

function moveBlockTo(
  block: DOMWrapper<HTMLElement>,
  { x, y }: { x: number; y: number }
) {
  const position = getPosition(block)
  block.trigger('mousedown', { clientX: position.x, clientY: position.y })
  document.dispatchEvent(
    new MouseEvent('mousemove', { clientX: x, clientY: y })
  )
  document.dispatchEvent(new MouseEvent('mouseup'))
}
