import { defineConfig } from 'vitest/config'
import vue from '@vitejs/plugin-vue'
import Unocss from 'unocss/vite'

export default defineConfig({
  base: '/jihu-frontend-code-test/',
  plugins: [vue(), Unocss()],
  test: { environment: 'jsdom' },
})
